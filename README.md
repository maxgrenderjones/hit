# Hit

As bitbucket [sunsets mercurial support](https://bitbucket.org/blog/sunsetting-mercurial-support-in-bitbucket) (amidst a thousand other paper cuts), it seems the great distributed scm bikeshedding wars are drawing to a close. And git won. If that makes you sad, `hit` might be for you. (That said, ❤️ to [heptapod](https://heptapod.net/)).

What mercurial got right:

- The porcelain (i.e. the command-line interface)
- The separate concepts of named branches (live forever) and bookmarks (ephemeral ways to take short-lived lines of development)
- The leak-free nature of the abstraction between the mercurial and its data store. I have _never_ needed to know how mercurial is implemented internally and this has never been a problem.

What git got right:

- Being written by linus and adopted for the linux kernel
- Being adopted by github
- Integrations (likely some combination of being more widely adopted, the simplicity and stability of the on-disc format and the existence of multiple implementations in multiple languages such as [libgit2](https://libgit2.org/), [isomorphic-git](https://github.com/isomorphic-git/isomorphic-git), [jGit](http://www.eclipse.org/jgit/))
- Being so inscrutable and such a leaky abstraction that the web is littered with 'a beginners guide to `git`' tutorials, as mountaineers who climbed the vertical wall of learning attempted to provide pitons for others (A similar strategy works well for [vim](https://www.vim.org/)). In my experience, the average mercurial user read Joel Spoelsky's hg init tutorial and was done (the original seems to have [fallen off the internet](https://news.ycombinator.com/item?id=20748173), but here's another [hginit](https://farley.io/hginit2/)).

And unfortunately network effects took it from there. But all is not lost. `hit` attempts to present the mercurial commandline interface and translates your commands into the `git` equivalents (to see what commands it runs use `hit -v ...`). This means you can wallow in denial and nostalgia, while still interacting directly with `git` repositories...

```sh
> hit help
Usage:
  hit clone <url> [<destination>]
  hit init [<destination>]
  hit pull [<repository>]
  hit push [<repository>]
  hit commit|ci [<file>]...
  hit backout
  hit graft <rev>...
  hit branch <name>
  hit rebase
  hit branch [<name>]
  hit bookmarks|branches
  hit tag <name>
  hit tags
  hit cat <file>
  hit diff [<file>]
  hit log|history [<file>]
  hit add <file>...
  hit forget <file>...
  hit remove|rm <file>...
  hit revert <file>...
  hit shelve
  hit status|st [<file>]...
  hit summary|sum
  hit update|up|checkout|co [<revision>]
  hit unshelve
  hit strip <commit>
  hit config|showconfig
  hit help
  hit version

Repository management:
  clone                     Clone a local or remote repository

Repository creation:
  init                      Create a new repository in the given directory

Remote repository management:
  pull                      Pull changes from another repository
  push                      Push changes to another repository

Change creation:
  commit, ci                Commit changes to files
  backout                   Reverse effect of earlier changeset
  graft                     Copy changes from other branches onto the current
                            branch
  branch                    Create a new branch or list branches
  branch                    Create a new branch or list branches

Change manipulation:
  rebase                    Move changeset (and descendents) to a different
                            branch

Change organization:
  bookmarks, branches       List branches
  tag                       Add tags to the current revision
  tags                      List tags

File content management:
  cat                       Output the current or given revision of files
  diff                      Show the difference between two points in
                            development

Change navigation:
  log, history              Show revision history of entire repository or files

Working directory management:
  add                       Add files
  forget                    Forget any changes that have been added
  remove, rm                Remove the specified file(s) on the next commit
  revert                    Revert files to a specific version
  shelve                    Save and set aside changes from the working
                            directory
  status, st                Show the status of the local repository
  summary, sum              Summarize working directory state
  update, up, checkout, co  Update to a specific version of the repository
  unshelve                  Restore a shelved change to the working directory

Repository maintenance:
  strip                     Remove commits from the local repository

Help:
  config, showconfig        Show config
  help                      Show help message
  version                   Version

General options:
  -v, --verbose             Verbose output
```

## `git` commands with no `hit` equivalent

Some commands in `git` do not have a direct mercurial translation

- Setting remote URLs: `git remote add <alias> <url>` (This is set by hand in .hgrc)

## Workflow differences

Migrants from mercurial to `git` have two mental leaps to make: the first from a mercurial model of how distributed source code management works; the second to understand the seemingly arcane invocations needed to make `git` do anything sensible. `hit` exists to help you with the commandline transition; the following is intended to help you with your mental model.

### branches are dead, long live the bookmark

`git` does not have the equivalent of the mercurial branch - a way of tagging a line of commits to say that are part of the same thing. Rather, what `git` calls branches are little more than little more than pointers to a commit, and so I will use bookmarks to describe them, as the equivalent concept in the mercurial world. Indeed if you have used `hg-git` to work between a mercurial and a `git` repository, you will have seen that only bookmarks are carried across and appear on the `git` side as branches.

One consequence of this is that there is no concept of 'being on a branch' (at least not as a mercurial user would think about it). You are either on a bookmark (which always has the alias of `HEAD`), or you are not. And if not, you are in what is called 'detatched head state'. `git` only maintains commits that are on a line of development that terminate in a bookmark / `git` branch, so if you start from 'detached head state' and add  commits without adding a new bookmark your commits are liable to be garbage collected (i.e. deleted) at some point in the future. In other words `git` will simply clean up unnamed branches.

### Remotes everywhere

An early impression is likely to be that `git` seems strangely obsessed with remotes. Convention dictates that you have two remotes set up - `origin`, which points to your fork of the repository on the hosting server, and `upstream` which points to the canonical repository. To create a remote, run `git remote add <name> <url>` (for now - there is no `hit` equivalent command to do this).

Remotes also come into play when thinking about how bookmarks work. When you `hit pull` the latest changes from a remote, `git` maintains a local record of where any bookmarks point on the remote, but no changes are made to where your local equivalents point. In order to create a link between a local bookmark called `topic` and a bookmark called `topic` on the remote called `origin`, you would run `git breanch --set-upstream-to=topic`. 

### Pulling from upstream

After pulling the latest changes from upstream, your local bookmarks will not be updated to match the remote ones (sort of like diverent bookmarks). So to pull in the latest goodness on the `devel` bookmark, run:

```sh
> hit pull upstream -b devel  # Get the latest changes to `devel` from `upstream`
> hit update upstream/devel   # Update your code to the `upstream/devel` bookmark
> hit branch -f devel         # Move your `devel` bookmark to here
```

### Other migration guides

- [git for longtime mercurial users](https://medium.com/@tmvvr/git-for-longtime-mercurial-users-c41f37352fca)
- [git for mercurial users](https://til.secretgeek.net/git/git_for_mercurial_users.html)