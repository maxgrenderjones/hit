import algorithm
import md5
import os
import osproc
import posix_utils
import strformat
import strutils
import sugar
import unittest

proc tree(path="./"): string =
    let files: seq[string] = collect(newSeq):
        for name in walkDirRec(path):
            if ".git" in name or ".hg" in name:
                continue
            if existsFile(name):
                name
    let lines: seq[string] = collect(newSeq):
        for f in sorted(files):
            fmt"{readFile(f).toMD5()} {f}"
    lines.join("\n")

template withTempDir(prefix: string, code: untyped): untyped =
    let tempdirname {.inject.} = absolutePath(mkdtemp(prefix))
    try:
        code
    finally:
        removeDir(tempdirname)

template withCurrentDir(path: string, code: untyped): untyped =
    let cwd = getCurrentDir()
    try:
        setCurrentDir(path)
        code
    finally:
        setCurrentDir(cwd)

template doIn(paths: openarray[string], code: untyped): untyped =
    for path in paths:
        withCurrentDir(path):
            code

template getIn(path: string, code: untyped): untyped =
    withCurrentDir(path):
        code

proc runIn(path: string, command: string): tuple[output: TaintedString, status: int] =
    withCurrentDir(path):
        let (output, status) = execCmdEx(command)
        result = (output, status)

proc mirror(command: string) =
    let hg_command = runIn("hg", "hg " & command)
    if hg_command.status!=0:
        echo(fmt"> hg {command}")
        echo(indent(hg_command.output.strip, 2))
    check(hg_command.status==0)
    let hit_command = runIn("git", "hit -v " & command)
    echo(fmt"> hit -v {command}")
    echo(indent(hit_command.output.strip, 2))
    check(hit_command.status==0)
    let hg_tree = getIn("hg"):
        tree()
    let hit_tree = getIn("git"):
        tree()
    if not (hg_tree==hit_tree):
        echo runIn("hg", "hg log -G").output
        echo hg_tree
        echo runIn("git", "hit log -G").output
        echo hit_tree
        # Hard quit (leaving temporary directory around to allow inspection)
        require(hg_tree==hit_tree)


if isMainModule:

    suite "Hello world tests":
        test "Basic workflow":
            withTempDir("hit"):
                withCurrentDir(tempdirname):
                    echo "[Initialise the repositories]"
                    createDir("hg")
                    createDir("git")
                    mirror("init")
                    echo "[Create the first commit on the master branch]"
                    doIn(["hg", "git"]):
                        writeFile("1", "one")
                    mirror("add 1")
                    mirror("commit 1 -m 'First commit'")
                    check(runIn("hg", "hg bookmark master").status==0)
                    mirror("log -G")
                    echo "[Create a second commit on a new branch called 'second' that branches off master]"
                    mirror("bookmark second")
                    doIn(["hg", "git"]):
                        writeFile("2", "two")
                    mirror("add 2")
                    mirror("commit 2 -m 'Second commit'")
                    mirror("bookmark")
                    echo "[Create a third commit on the master branch]"
                    mirror("update master")
                    mirror("bookmark")
                    doIn(["hg", "git"]):
                        writeFile("3", "three")
                    mirror("add 3")
                    mirror("commit 3 -m 'third commit'")
                    echo "[Create a fourth commit on a new branch called 'fourth' that branches off master]"
                    mirror("update master")
                    mirror("bookmark fourth")
                    doIn(["hg", "git"]):
                        writeFile("4", "four")
                    mirror("add 4")
                    mirror "commit 4 -m 'Fourth commit'"
                    echo "[Rebase the branch 'fourth' onto the branch 'second']"
                    mirror "rebase -s fourth -d second"
                    echo "[Merge the branch 'second' onto the master branch]"
                    # Merge the second branch into the master branch
                    mirror("update master")
                    mirror("merge second")
                    mirror("log -G")