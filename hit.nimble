# from src/commands/version as v import VERSION

version       = "0.1.0"
author        = "Max Grender-Jones"
description   = "git porcelain for mercurial refugees"
license       = "MIT"
srcDir        = "src"
bin           = @["hit"]

requires "therapist > 0.1.0"

task pipelines, "Builds hit for multiple targets":
    when defined(osx):
        exec "nim c -o:hit.osx src/hit.nim"
    exec "bitlines -x hit.amd64 -x hit.arm64 -x hit.armhf -x hit.exe"