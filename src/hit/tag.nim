import therapist

import common 

const PROLOG = """
Name a particular revision using <name>.

Tags are used to name particular revisions of the repository and are very
useful to compare different revisions, to go back to significant earlier
versions or to mark branch points as releases, etc. Changing an existing
tag is normally disallowed; use -f/--force to override.

If no revision is given, the parent of the working directory is used.

Notes for hit:

 - Unlike mercurial, tags in git immediately exist, so do not have the issue
where the tag does not exist until the following commit
"""


type  
    TagSpec = tuple[
        name: StringArg,
        message: StringArg,
        revision: StringArg,
        remove: CountArg,
        force: CountArg,
        help: HelpArg,
    ]

proc getTagSpec(): TagSpec = (
    name: newStringArg("<name>", help="Name of the tag"),
    message: newStringArg("-m, --message", help="Use given tag message", helpvar="text"),
    revision: newStringarg("-r, --rev", help="Revision to tag"),
    remove: newCountArg("--remove", help="Remove a tag"),
    force: newCountArg("-f, --force", help="Force tag"),
    help: newHelpArg(),
)

proc runTagCommand(spec: TagSpec, verbose: bool) =
    var options: seq[string]
    var args: seq[string]
    if spec.force.seen:
        options.add("-f")
    if spec.remove.seen:
        options.add("--delete")
    if spec.message.seen:
        options &= @["--message", spec.message.value]
    if spec.revision.seen:
        args.add(spec.revision.value)
    quit run(verbose, @["git", "tag"] & options & spec.name.value & args)

proc getTagCommand*(options: HitOptions): CommandArg =
    newCommandArg("tag", getTagSpec(), help="Add tags to the current revision", prolog=PROLOG, group= $gOrganization, handle=makeHandler(runTagCommand, options))
