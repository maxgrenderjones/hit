import therapist

import common

type
    BackoutSpec* = tuple [
        revision: StringArg,
        help: HelpArg,
    ]

proc getBackoutSpec*(): BackoutSpec = (
    revision: newStringArg("-r, --rev", help="Revision to reverse"),
    help: newHelpArg()
)

proc runBackoutCommand*(spec: BackoutSpec, verbose: bool) =
    let revision = if spec.revision.seen: spec.revision.value else: currentRev(verbose)
    quit run(verbose, "git", "revert", revision)

proc getBackoutCommand*(options: HitOptions): CommandArg =
    newCommandArg(@["backout"], getBackoutSpec(), help="Reverse effect of earlier changeset", group= $gManipulation, handle=makeHandler(runBackoutCommand, options))