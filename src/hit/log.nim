import therapist

import common

type
    LogSpec* = tuple[
        help: HelpArg,
        file: StringArg,
        graph: CountArg,
        limit: IntArg,
    ]

proc getLogSpec*(): LogSpec = (
    help: newHelpArg(),
    file: newStringArg("<file>", help="File to review", optional=true),
    graph: newCountArg("-G, --graph", help="show the revision DAG"),
    limit: newIntArg("-l, --limit", help="limit number of changes displayed", helpvar="num")
)

proc runLogCommand*(spec: LogSpec, verbose: bool) =
    var args: seq[string]
    var options = @["--decorate"]
    if spec.graph.seen:
        options.add("--graph")
    if spec.limit.seen:
        options &= @["-n", $spec.limit.value]
    if not false: # spec.branch.seen
        options.add("--all")
    if spec.file.seen:
        args.add(spec.file.value)
    quit run(verbose, @["git", "log"] & options & args)

proc getLogCommand*(options: HitOptions): CommandArg =
    newCommandArg(@["log", "history"], getLogSpec(), help="Show revision history of entire repository or files", group= $gNavigation, handle=makeHandler(runLogCommand, options))
