import therapist

import common

type
    ShelveSpec = tuple[
        unknown: CountArg,
        cleanup: CountArg,
        delete: CountArg,
        list: CountArg,
        name: StringArg,
    ]

    UnshelveSpec = tuple[
        name: StringArg,
    ]


proc getShelveSpec(): ShelveSpec = (
    unknown: newCountArg("-u, --unknown", help="Store unknown files in the shelve"),
    cleanup: newCountArg("--cleanup", help="Delete all shelved changes"),
    delete: newCountArg("-d, --delete", help="Delete the named shelved change(s)"),
    list: newCountArg("-l, --list", help="List current shelves"),
    name: newStringArg("-n, --name", help="Restore shelved changes with given name"),
)

proc getUnshelveSpec(): UnshelveSpec = (
    name: newStringArg("-n, --name", help="Restore shelved changes with given name")
)

proc runShelveCommand(spec: ShelveSpec, verbose: bool) =
    var options: seq[string]
    if spec.cleanup.seen:
        quit run(verbose, "git", "stash", "clear")
    elif spec.list.seen:
        quit run(verbose, "git", "stash", "list")
    elif spec.delete.seen:
        if not spec.name.seen:
            error("Missing name of shelve to delete", 1)
        quit run(verbose, "git", "stash", "drop", spec.name.value)
    # We're creating a shelve
    if spec.unknown.seen:
        options.add("--include-untracked")
    quit run(verbose, @["git", "stash", "push"] & options)

proc runUnshelveCommand(spec: UnshelveSpec, verbose: bool) =
    var args: seq[string]
    if spec.name.seen:
        args.add(spec.name.value)
    quit run(verbose, @["git", "stash", "pop"] & args)

proc getShelveCommand*(options: HitOptions): CommandArg =
    newCommandArg("shelve", getShelveSpec(), help="Save and set aside changes from the working directory", group= $gWorking, handle=makeHandler(runShelveCommand, options))

proc getUnShelveCommand*(options: HitOptions): CommandArg =
    newCommandArg("unshelve", getUnshelveSpec(), help="Restore a shelved change to the working directory", group= $gWorking, handle=makeHandler(runUnshelveCommand, options))
