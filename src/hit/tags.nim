import therapist

import common

const PROLOG = """
Lists the tags in the repository
"""

type
    TagsSpec = tuple[
        help: HelpArg
    ]

proc getTagsSpec(): TagsSpec = (
    help: newHelpArg(), 
)

proc runTagsCommand(spec: TagsSpec, verbose: bool) =
    quit run(verbose, "git", "tag", "--list")

proc getTagsCommand*(options: HitOptions): CommandArg =
   newCommandArg("tags", getTagsSpec(), help="List tags", prolog=PROLOG, group= $gOrganization, handle=makeHandler(runTagsCommand, options)) 
