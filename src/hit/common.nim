import os
import osproc
import pegs
import sugar
import strformat
import strutils
import terminal

import therapist

export pegs.match

let REMOTE_BRANCH* = peg""" remote_branch <- {\w+} '/' {\w+}"""

type

    groupType* = enum 
        gCreation = "Repository creation",
        gRemote = "Remote repository management",
        gChange = "Change creation",
        gManipulation = "Change manipulation",
        gOrganization = "Change organization"
        gContent = "File content management",
        gNavigation = "Change navigation",
        gRepository = "Repository management",
        gWorking = "Working directory management",
        gMaintenance = "Repository maintenance",
        gHelp = "Help",
        gOptions = "General options",
    
    HitOptions* = tuple[
        verbose: CountArg
    ]
    
proc makeHandler*[S](handle: proc(commandSpec: S, verbose: bool), options: HitOptions): proc (commandSpec: S) =
    result = (commandSpec: S) => handle(commandSpec, verbose=options.verbose.seen)

proc run*(verbose: bool, commands: varargs[string]): int =
    let command = quoteShellCommand commands
    if verbose:
        echo command
    execShellCmd command

proc capture(verbose: bool, commands: varargs[string]): tuple[output: TaintedString, exitCode: int] =
    let command = quoteShellCommand commands
    if verbose:
        echo command
    let (output, exitCode) = execCmdEx command
    return (output.strip(), exitCode)

template `&&`*(command1: untyped, command2: untyped): int =
    let status = command1
    if status!=0:
        status
    else:
        command2

proc error*(message: string, status: int) =
    if isatty(stdout):
        styledEcho fgRed, message, fgDefault
        quit(status)
    else:
        quit(message, status)

proc dirty*(verbose: bool): bool =
    ## Returns true if the working directory contains modifications of tracked files
    (run(verbose, "git", "diff", "--quiet") && run(verbose, "git", "diff", "--cached", "--quiet"))!=0

proc currentRev*(verbose: bool): string =
    ## Returns the current revision of the working directory
    let (revision, status) = capture(verbose, "git", "log", "-n", "1", "--pretty=format:%H")
    if status!=0:
        error(revision, status)
    revision.strip()

proc currentBranch*(verbose: bool): string =
    let (branch, status) = capture(verbose, "git", "rev-parse", "--abbrev-ref", "HEAD")
    if status!=0:
        error(branch, status)
    branch.strip()

proc upstreamBranch*(verbose: bool): tuple[output: TaintedString, status: int] =
    let (branch, bstatus) = capture(verbose, "git", "symbolic-ref", "-q", "HEAD")
    if bstatus!=0:
        return (branch, bstatus)
    let (remote, rstatus) = capture(verbose, "git", "for-each-ref", "--format=%(upstream:short)", branch)
    return (remote, rstatus)

proc existsBranch*(verbose: bool, name: string): bool =
    let (_, status) = capture(verbose, "git", "show-ref", "--quiet", fmt"refs/heads/{name}")
    status==0