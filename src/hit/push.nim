import therapist

import common

type
    PushSpec* = tuple[
        repository: StringArg,
        branch: StringArg,
        force: CountArg,
        help: HelpArg
    ]

proc getPushSpec*(): PushSpec = (
    repository: newStringArg("<repository>", help="Name or URL of repository to push to", optional=true),
    branch: newStringArg("-b, -B, --branch, --bookmark", help="Name of the branch to push", optional=true),
    force: newCountArg("-f, --force", help="Force push"),
    help: newHelpArg(),
)


proc runPushCommand*(spec: PushSpec, verbose: bool) = 
    var options: seq[string]
    if spec.force.seen:
        options.add("--force")
    var args: seq[string]
    if spec.repository.seen:
        args.add(spec.repository.value)
    if spec.branch.seen:
       args.add(spec.branch.value)
    let upstream = upstreamBranch(verbose)
    if upstream.output=="":
        options.add("--set-upstream")
    quit run(verbose, @["git", "push"] & options & args)


proc getPushCommand*(options: HitOptions): CommandArg =
    newCommandArg(@["push"], getPushSpec(), help="Push changes to another repository", group = $gRemote, handle=makeHandler(runPushCommand, options))