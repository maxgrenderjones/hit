import therapist

import common

type
    CommitSpec* = tuple[
        help: HelpArg,
        file: PathArg,
        message: StringArg,
    ]

proc getCommitSpec*(): CommitSpec = (
    help: newHelpArg(),
    file: newPathArg(@["<file>"], help="File(s) to commit", multi=true, optional=true),
    message: newStringArg(@["-m", "--message"], help="Commit message"),
)

proc runCommitCommand*(spec: CommitSpec, verbose: bool) =
    var options: seq[string]
    var args: seq[string]
    if spec.message.seen:
        options &= ["--message", spec.message.value]
    if spec.file.seen:
        args &= spec.file.values
        quit run(verbose, @["git", "commit"] & options & args)
    else:
        quit (run(verbose, @["git", "add", "--update"]) && run(verbose, @["git", "commit"] & options & args))

proc getCommitCommand*(options: HitOptions): CommandArg =
    newCommandArg(@["commit", "ci"], getCommitSpec(), help="Commit changes to files", group= $gChange, handle=makeHandler(runCommitCommand, options))