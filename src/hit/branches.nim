import therapist

import common

type 
    BranchesSpec* = tuple[
        help: HelpArg
    ]

proc getBranchesSpec*(): BranchesSpec = (
    help: newHelpArg()
)

proc runBranchesCommand*(spec: BranchesSpec, verbose: bool) =
    quit run(verbose, "git", "branch", "--list")

proc getBranchesCommand*(options: HitOptions): CommandArg =
    newCommandArg("bookmarks, branches", getBranchesSpec(), help="List branches", group= $gOrganization, handle=makeHandler(runBranchesCommand, options))