import therapist

import uri

import common

type
    CloneSpec* = tuple[
        url: URLArg,
        destination: StringArg,
        noupdate: CountArg,
        help: HelpArg
    ]

proc getCloneSpec*(): CloneSpec = (
    url: newURLArg(@["<url>"], help="URL of local/remote repository"),
    destination: newStringArg(@["<destination>"], help="Destination for cloned repository", optional=true, defaultVal="./"),
    noupdate: newCountArg(@["-U", "--noupdate"], help="Do not check out any files"),
    help: newHelpArg(),
)

proc runCloneCommand*(spec: CloneSpec, verbose: bool) = 
    quit if spec.noupdate.seen:
        run verbose, "git", "clone", "--no-checkout", $spec.url.value, spec.destination.value
    else:
        run verbose, "git", "clone", $spec.url.value, spec.destination.value

proc getCloneCommand*(options: HitOptions): CommandArg =
    newCommandArg(@["clone"], getCloneSpec(), help="Clone a local or remote repository", group= $gCreation, handle=makeHandler(runCloneCommand, options))