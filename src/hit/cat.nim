import strformat

import therapist

import common

type
    CatSpec = tuple[
        revision: StringArg,
        file: StringArg,
        help: HelpArg,
    ]

proc getCatSpec*(): CatSpec = (
    revision: newStringArg("-r, --rev", help="Print the given revision"),
    file: newStringArg("<file>", help="File to output"),
    help: newHelpArg()
)

proc runCatCommand*(spec: CatSpec, verbose: bool) =
    let revision = if spec.revision.seen: spec.revision.value else: currentRev(verbose)
    quit run(verbose, "git", "show", fmt"{revision}:{spec.file.value}")

proc getCatCommand*(options: HitOptions): CommandArg =
    newCommandArg(@["cat"], getCatSpec(), help="Output the current or given revision of files", group= $gContent, handle=makeHandler(runCatCommand, options))