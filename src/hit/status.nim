import therapist

import common

type
    StatusSpec* = tuple[
        files: PathArg,
        help: HelpArg,
    ]

proc getStatusSpec*(): StatusSpec = (
    files: newPathArg(@["<file>"], help="Files to show the status of", defaultVal="./", optional=true, multi=true),
    help: newHelpArg()
)

proc runStatusCommand*(spec: StatusSpec, verbose: bool) =
    quit run(verbose, @["git", "status", "--short", "--untracked-files=all"] & spec.files.values)

proc getStatusCommand*(options: HitOptions): CommandArg =
    newCommandArg(@["status", "st"], getStatusSpec(), help="Show the status of the local repository", group = $gWorking, handle=makeHandler(runStatusCommand, options))