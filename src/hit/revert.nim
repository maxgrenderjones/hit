import therapist

import common

type
    RevertSpec* = tuple[
        files: StringArg,
        help: HelpArg,
    ]

proc getRevertSpec*(): RevertSpec = (
    files: newStringArg(@["<file>"], help="Files to revert", multi=true),
    help: newHelpArg()
)

proc runRevertCommand*(spec: RevertSpec, verbose: bool) =
    quit run(verbose, @["git", "checkout", "HEAD", "--"] & spec.files.values)

proc getRevertCommand*(options: HitOptions): CommandArg =
    newCommandArg(@["revert"], getRevertSpec(), help="Revert files to a specific version", group = $gWorking, handle=makeHandler(runRevertCommand, options))