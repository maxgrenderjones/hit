import therapist

import common

type
    ForgetSpec* = tuple[
        files: StringArg,
        help: HelpArg,
    ]

proc getForgetSpec*(): ForgetSpec = (
    files: newStringArg(@["<file>"], help="Files to forget changes for", multi=true),
    help: newHelpArg()
)

proc runForgetCommand*(spec: ForgetSpec, verbose: bool) =
    quit run(verbose, @["git", "reset", "HEAD"] & spec.files.values)

proc getForgetCommand*(options: HitOptions): CommandArg =
    newCommandArg(@["forget"], getForgetSpec(), help="Forget any changes that have been added", group = $gWorking, handle=makeHandler(runForgetCommand, options))