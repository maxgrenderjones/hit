import strformat

import therapist

import common

type
    StripSpec* = tuple[
        help: HelpArg,
        commit: StringArg,
    ]

proc getStripSpec*(): StripSpec = (
    help: newHelpArg(),
    commit: newStringArg(@["<commit>"], help="Commit to be removed from the repository along with its descendents"),
)

proc runStripCommand*(spec: StripSpec, verbose: bool) =
    if dirty(verbose):
        error("Abort: Uncommitted changes", 1)
    let commit = spec.commit.value
    quit run(verbose, "git", "reset", "--hard", fmt"{commit}~")
    
proc getStripCommand*(options: HitOptions): CommandArg =
    newCommandArg(@["strip"], getStripSpec(), help="Remove commits from the local repository", group = $gMaintenance, handle=makeHandler(runStripCommand, options))