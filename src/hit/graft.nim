import strutils

import therapist

import common

const PROLOG = """
Given one or more existing commits, apply the change each one introduces,
recording a new commit for each. This requires your working tree to be clean
(no modifications from the HEAD commit).

Git equivalent: cherry-pick
"""

type
    GraftSpec = tuple[
        revision: StringArg,
        help: HelpArg
    ]

proc getGraftSpec(): GraftSpec = (
    revision: newStringArg("<rev>", help="Revision(s) to add to the current branch", multi=true),
    help: newHelpArg()
)

proc runGraftCommand(spec: GraftSpec, verbose: bool) =
    if dirty(verbose):
        error("Abort: working directory not clean", 1)
    quit run(verbose, @["git", "cherry-pick"] & spec.revision.values)

proc getGraftCommand*(options: HitOptions): CommandArg =
    newCommandArg("graft", getGraftSpec(), help="Copy changes from other branches onto the current branch", prolog=PROLOG, group= $gManipulation, handle=makeHandler(runGraftCommand, options))