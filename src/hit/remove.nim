import therapist

import common

type
    RemoveSpec = tuple[
        file: StringArg
    ]

proc getRemoveSpec(): RemoveSpec = (
    file: newStringArg("<file>", help="File(s) to remove", multi=true)
)

proc runRemoveCommand(spec: RemoveSpec, verbose: bool) =
    quit run(verbose, @["git", "rm"] & spec.file.values)

proc getRemoveCommand*(options: HitOptions): CommandArg =
   newCommandArg("remove, rm", getRemoveSpec(), help="Remove the specified file(s) on the next commit", group= $gWorking, handle=makeHandler(runRemoveCommand, options)) 
