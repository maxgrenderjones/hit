import therapist

import common

type
    DiffSpec* = tuple[
        file: StringArg,
        space: CountArg,
        help: HelpArg,
    ]

proc getDiffSpec*(): DiffSpec = (
    file: newStringArg(@["<file>"], help="File(s) to compare", optional=true),
    space: newCountArg("-w, --ignore-all-space", help="Ignore white space when comparing lines"),
    help: newHelpArg(),
)

proc runDiffCommand*(spec: DiffSpec, verbose: bool) =
    var options: seq[string]
    if spec.space.seen:
        options.add("--ignore-all-space")
    
    quit if not spec.file.seen:
        run(verbose, @["git", "diff"] & options) && run(verbose, @["git", "diff", "--staged"] & options)
    else:
        run(verbose, @["git", "diff"] & options & @["HEAD"] & spec.file.values)

proc getDiffCommand*(options: HitOptions): CommandArg =
    newCommandArg(@["diff"], getDiffSpec(), help="Show the difference between two points in development", group= $gContent, handle=makeHandler(runDiffCommand, options))