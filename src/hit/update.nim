import therapist

import common

const PROLOG = """
Update the repository's working directory to the specified changeset.

Notes for hit:
- If you try to update to <remote>/<branch> and <branch> does not exist as
a local remote, runs `git checkout --track <remote>/<branch>`. If <branch>
does exist then runs `git checkout <remote>/<branch>`, leaving you in detached
head state.
"""

type
    UpdateSpec* = tuple[
        rev: StringArg,
        revision: StringArg,
        check: CountArg,
        help: HelpArg,
    ]

proc getUpdateSpec*(): UpdateSpec = (
    rev: newStringArg("-r, --rev", help="revision"),
    revision: newStringArg(@["<revision>"], help="Revision to update to", defaultVal="HEAD", optional=true),
    check: newCountArg("-c, --check", help="Require clean working directory"),
    help: newHelpArg()
)

proc runUpdateCommand*(spec: UpdateSpec, verbose: bool) =
    if dirty(verbose):
        error("Abort: working directory is dirty", 1)
    let revision = if spec.rev.seen: spec.rev.value else: spec.revision.value
    var remote_branch: array[2, string]
    if revision.match(REMOTE_BRANCH, remote_branch) and not existsBranch(verbose, remote_branch[1]):
        quit run(verbose, "git", "checkout", "--track", revision)
    else:
        quit run(verbose, "git", "checkout", revision)

proc getUpdateCommand*(options: HitOptions): CommandArg =
    newCommandArg("update, up, checkout, co", getUpdateSpec(), help="Update to a specific version of the repository", prolog=PROLOG, group= $gWorking, handle=makeHandler(runUpdateCommand, options))