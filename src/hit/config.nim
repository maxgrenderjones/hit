import therapist

import common

type
    ConfigSpec* = tuple[
        help: HelpArg,
    ]

proc getConfigSpec*(): ConfigSpec = (
    help: newHelpArg()
)

proc runConfigCommand*(spec: ConfigSpec, verbose: bool) =
    quit run(verbose, @["git", "config", "--list"])

proc getConfigCommand*(options: HitOptions): CommandArg =
    newCommandArg("config, showconfig", getConfigSpec(), help="Show config", group= $gHelp, handle=makeHandler(runConfigCommand, options))

