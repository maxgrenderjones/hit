import strformat

import therapist

import common

type
    RebaseSpec* = tuple[
        source: StringArg, 
        dest: StringArg,
        help: HelpArg,
    ]

proc getRebaseSpec*(): RebaseSpec = (
    source: newStringArg("-s, --source", help="Rebase the changes between base and the specified changeset", helpvar="rev", required=true),
    dest: newStringArg("-d, --dest", help="Rebase onto specified changeset", helpvar="rev", required=true),
    help: newHelpArg()
)

proc runRebaseCommand*(spec: RebaseSpec, verbose: bool) =
    if dirty(verbose):
        error("Abort: working directory not clean", 1)
    let before = currentBranch(verbose)
    quit(run(verbose, "git", "checkout", spec.source.value) && 
        run(verbose, "git", "rebase", "--onto", spec.dest.value, fmt"{spec.source.value}^") &&
        run(verbose, "git", "checkout", before))

proc getRebaseCommand*(options: HitOptions): CommandArg =
    newCommandArg(@["rebase"], getRebaseSpec(), help="Move changeset (and descendents) to a different branch", group = $gManipulation, handle=makeHandler(runRebaseCommand, options))