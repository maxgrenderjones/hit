import strformat

import therapist 

import common 

const PROLOG = """
Set, show or delete the current branch name

With no argument, show the current branch name. With one argument, set the
working directory branch name (the branch will not exist in the repository
until the next commit). Standard practice recommends that primary
development take place on the 'default' branch.

Unless -f/--force is specified, branch will not let you set a branch name
that already exists.

Notes for hit:

- Unlike git, hit will automatically switch to the new branch once created
"""


type 
    BranchSpec = tuple[
        name: StringArg,
        force: CountArg,
        delete: tuple[
            gentle: CountArg,
            force: CountArg,
        ],
        help: HelpArg,
    ] 

proc getBranchSpec(): BranchSpec = (
    name: newStringArg("<name>", help="Name of new branch", optional=true),
    force: newCountArg("-f, --force", help="Force the action"),
    delete: (
        gentle: newCountArg("-d, --delete", help="Delete the branch"),
        force: newCountArg("-D", help="Short for --delete --force"),
    ),
    help: newHelpArg(),
)

proc runBranchCommand(spec: BranchSpec, verbose: bool) =
    var options: seq[string]
    if spec.name.value.match(REMOTE_BRANCH):
        options.add("--remotes")
    if not spec.name.seen:
        quit run(verbose, "git", "branch", "--list")
    if spec.force.seen or spec.delete.force.seen:
        options.add("--force")
    if spec.delete.gentle.seen or spec.delete.force.seen:
        quit run(verbose, @["git", "branch"] & options & @["--delete", spec.name.value])

    quit run(verbose, @["git", "branch"] & options & @[spec.name.value]) && 
        run(verbose, "git", "checkout", spec.name.value)

proc getBranchCommand*(options: HitOptions): CommandArg =
    newCommandArg("branch, bookmark", getBranchSpec(), help="Create a new branch or list branches", prolog=PROLOG, group= $gOrganization, handle=makeHandler(runBranchCommand, options))

