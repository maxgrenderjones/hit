import therapist 

import common 

const PROLOG = """
The current working directory is updated with all changes made in the
requested revision since the last common predecessor revision.

Files that changed between either parent are marked as changed for the
next commit and a commit must be performed before any further updates to
the repository are allowed. The next commit will have two parents.

If no revision is specified, the working directory's parent is a head
revision, and the current branch contains exactly one other head, the
other head is merged with by default. Otherwise, an explicit revision with
which to merge must be provided.

Notes for hit:

- Like mercurial, but unlike git, hit does not perform a merge commit
automatically (unless you have done a fast forward merge, in which case 
there is nothing to commit)
"""

type 
    MergeSpec = tuple[
        name: StringArg,
        ff: tuple[
            ff: CountArg,
            ffonly: CountArg,
        ],
        abort: CountArg,
        help: HelpArg,
    ] 

proc getMergeSpec(): MergeSpec = (
    name: newStringArg("<name>", help="Name of branch to merge in"),
    ff: (
      ff: newCountArg("--ff, --fast-forward", help="Performs a fast forward merge if possible"),
      ffonly: newCountArg("--ff-only, --fast-forward-only", help="Only merge if a fast forward merge is possible"),
    ),
    abort: newCountArg("--abort", help="Abort the ongoing merge"),
    help: newHelpArg(),
)

proc runMergeCommand(spec: MergeSpec, verbose: bool) =
    if spec.abort.seen:
        quit run(verbose, "git", "merge", "--abort")
    elif spec.ff.ff.seen:
        quit run(verbose, "git", "merge", "--ff", spec.name.value)
    elif spec.ff.ffonly.seen:
        quit run(verbose, "git", "merge", "--ff-only", spec.name.value)
    else:
      quit run(verbose, "git", "merge", "--no-ff", "--no-commit", spec.name.value)

proc getMergeCommand*(options: HitOptions): CommandArg =
    newCommandArg("merge", getMergeSpec(), help="Merge branches together", prolog=PROLOG, group= $gManipulation, handle=makeHandler(runMergeCommand, options))

