import therapist

import common

type
    InitSpec* = tuple[
        destination: StringArg,
        help: HelpArg,
    ]

proc getInitSpec*(): InitSpec = (
    destination: newStringArg(@["<destination>"], help="Location of new repository", optional=true, defaultVal="./"),
    help: newHelpArg(),
)


proc runInitCommand*(spec: InitSpec, verbose: bool) = 
    quit run(verbose, @["git", "init"])

proc getInitCommand*(options: HitOptions): CommandArg = 
    newCommandArg(@["init"], getInitSpec(), help="Create a new repository in the given directory", group = $gCreation, handle=makeHandler(runInitCommand, options))