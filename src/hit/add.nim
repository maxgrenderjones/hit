import therapist

import common

type
    AddSpec* = tuple[
        files: PathArg,
        help: HelpArg,
    ]

proc getAddSpec*(): AddSpec = (
    files: newPathArg(@["<file>"], help="Files to add", multi=true),
    help: newHelpArg()
)

proc runAddCommand*(spec: AddSpec, verbose: bool) =
    quit run(verbose, @["git", "add"] & spec.files.values)

proc getAddCommand*(options: HitOptions): CommandArg =
    newCommandArg(@["add"], getAddSpec(), help="Add files", group= $gWorking, handle=makeHandler(runAddCommand, options))
