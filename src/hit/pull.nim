import strutils

import therapist

import common

let PROLOG = """
Pull changes from a remote repository to a local one

This finds all changes from the repository at the specified path or URL
and adds them to a local repository. By default, this does not update
the copy of the project in the working directory.
""".strip()

type
    PullSpec* = tuple[
        repository: StringArg,
        branch: StringArg,
        update: CountArg,
        help: HelpArg
    ]

proc getPullSpec*(): PullSpec = (
    repository: newStringArg("<repository>", help="Name or URL of repository to push to", defaultVal="origin", optional=true),
    branch: newStringArg("-b, -B, --branch, --bookmark", help="Name of the branch to pull", helpvar="branch"),
    update: newCountArg("-u, --update", help="Update to HEAD after pulling changes"),
    help: newHelpArg(),
)

proc runPullCommand*(spec: PullSpec, verbose: bool) =
    var options: seq[string]
    var args: seq[string]
    if spec.repository.seen:
        args.add(spec.repository.value)
        if spec.branch.seen:
            args.add(spec.branch.value)
    quit run(verbose, @["git", "fetch"] & options & args)

proc getPullCommand*(options: HitOptions): CommandArg =
    newCommandArg(@["pull"], getPullSpec(), help="Pull changes from another repository", prolog=PROLOG, group = $gRemote, handle=makeHandler(runPullCommand, options))