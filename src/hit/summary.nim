import therapist

import common

type
    SummarySpec* = tuple[
        help: HelpArg,
    ]

proc getSummarySpec*(): SummarySpec = (
    help: newHelpArg(),
)

proc runSummaryCommand*(spec: SummarySpec, verbose: bool) = 
    quit run(verbose, @["git", "log", "-1"])

proc getSummaryCommand*(options: HitOptions): CommandArg =
    newCommandArg(@["summary", "sum"], getSummarySpec(), help="Summarize working directory state", group = $gWorking, handle=makeHandler(runSummaryCommand, options))