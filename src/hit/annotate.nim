import strformat

import therapist

import common

const PROLOG = """
List changes in files, showing the revision id responsible for each line.

This command is useful for discovering when a change was made and by whom.

Without the -a/--text option, annotate will avoid processing files it
detects as binary. With -a, annotate will annotate the file anyway,
although the results will probably be neither useful nor desirable.

Notes for hit:

- Implemented using `git blame`, not `git annotate`
"""

type
    AnnotateSpec = tuple[
        file: StringArg,
        revision: StringArg,
        name: CountArg,
        line: Countarg,
        space: CountArg,
        help: HelpArg,
    ]

proc getAnnotateSpec*(): AnnotateSpec = (
    file: newStringArg("<file>", help="File to annotate"),
    revision: newStringArg("-r, --rev", help="Annotate the specified revison"),
    name: newCountarg("-f, --file", help="List the filename"),
    line: newCountArg("-l, --line-number", help="Show line number at the first appearance"),
    space: newCountarg("-w, --ignore-all-space", help="Ignore white space when comparing lines"),
    help: newHelpArg()
)

proc runAnnotateCommand*(spec: AnnotateSpec, verbose: bool) =
    var options: seq[string]
    var args: seq[string]
    if spec.revision.seen:
        args.add(spec.revision.value)
    if spec.name.seen:
        options.add("--show-name")
    if spec.line.seen:
        options.add("--show-number")
    if spec.space.seen:
        options.add("-w")
    quit run(verbose, @["git", "blame"] & options & args & @["--", spec.file.value])

proc getAnnotateCommand*(options: HitOptions): CommandArg =
    newCommandArg("annotate, blame", getAnnotateSpec(), help="Output the current or given revision of files", group= $gContent, handle=makeHandler(runAnnotateCommand, options))