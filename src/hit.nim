import therapist

import hit/add
import hit/annotate
import hit/backout
import hit/branch
import hit/branches
import hit/cat
import hit/clone
import hit/common
import hit/config
import hit/commit
import hit/diff
import hit/forget
import hit/graft
import hit/init
import hit/log
import hit/merge
import hit/pull
import hit/push
import hit/rebase
import hit/remove
import hit/shelve
import hit/status
import hit/strip
import hit/summary
import hit/tag
import hit/tags
import hit/revert
import hit/update
import hit/version

const
    PROLOG="""The porcelain for git that asks: what would mercurial do?"""

let options = (
    verbose: newCountArg(@["-v", "--verbose"], help="Verbose output", group = $gOptions)
)


let spec* = (
    clone:      getCloneCommand(options),
    init:       getInitcommand(options),
    # incoming
    # outgoing
    # paths
    pull:       getPullCommand(options),
    push:       getPushCommand(options),
    # serve
    commit:     getCommitCommand(options),
    backout:    getBackoutCommand(options),
    graft:      getGraftCommand(options),
    # histedit
    merge:      getMergeCommand(options),
    rebase:     getRebaseCommand(options),
    branch:     getBranchCommand(options),
    branches:   getBranchesCommand(options),
    tag:        getTagCommand(options),
    tags:       getTagsCommand(options),
    annotate:   getAnnotateCommand(options),
    cat:        getCatCommand(options),
    # copy
    diff:       getDiffCommand(options),
    # grep
    # bisect
    # heads - not really a thing in git
    # identify
    log:        getLogCommand(options), 
    add:        getAddCommand(options), 
    # addremove
    # files
    forget:     getForgetCommand(options),
    remove:     getRemoveCommand(options), 
    # rename
    # resolve
    revert:     getRevertCommand(options),
    # root
    shelve:     getShelveCommand(options),
    status:     getStatusCommand(options),
    summary:    getSummaryCommand(options),
    unshelve:   getUnShelveCommand(options),
    update:     getUpdateCommand(options),
    # archive
    # bundle
    # export
    # import
    # unbundle
    strip:      getStripCommand(options),      
    config:     getConfigCommand(options),
    help:       newHelpCommandArg(group = $gHelp),
    version:    newMessageCommandArg(@["version"], message=VERSION, help="Version", group = $gHelp),
    verbose:    options.verbose,
)

if isMainModule:

    # Order determined by order in which they appear in hg --help
    
    spec.parseOrQuit(prolog=PROLOG)
    echo "Error: No command provided"
    quit(spec.render_help(prolog=PROLOG))
